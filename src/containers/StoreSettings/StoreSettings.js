import React,{Component} from 'react';
import StoreSetting from '../../components/Store/StoreSetting/StoreSetting';
import PaymentSetting from '../../components/Store/PaymentSetting/PaymentSetting';
import Tips from '../../components/Store/Tips/Tips';
import classes from './StoreSettings.css';
import axios from 'axios';
import querySearch from "query-string";

class Storeettings extends Component{  
		

		state = {
			merchantStoreData : null,
			showTips:false,
            showSuccessMessage :'',
		}

      	componentDidMount(){
      		const cloverParseData = querySearch.parse(this.props.location.search); 
      		const merchantUrl = `http://159.65.134.223:8089/external/1/ext_restaurant/${cloverParseData.merchant_id}/`;
      		const storeData = axios.get( merchantUrl )
			  	.then((response) => { 
			  		this.setState({merchantStoreData: response.data});
			    },
			    (error) => {
			      console.log("error",error);
			      // const postData = this.createMerchantStore(cloverParseData);
			    }
			  ); 
      	}

    	createMerchantStore = ( cloverParseData ) =>{

			axios.post('http://159.65.134.223:8089/external/1/restaurant/', {

				merchantId : cloverParseData.merchant_id,
                code : cloverParseData.code,

            }).then(function(response){
            	return response;
			}).catch(function(error){
                console.log("error.message",error.message);
                //this.setState({showSuccessMessage : error.message });
            });
        }

      	submitMerchantSettings=()=>{
      	  const merchantSettingsContent = {
      	  	addressId: this.state.merchantStoreData.addressId,
			boardingStatus: this.state.merchantStoreData.boardingStatus,
			caption: this.state.merchantStoreData.caption,
			contact: this.state.merchantStoreData.contact,
			email: this.state.merchantStoreData.email,
			phone: this.state.merchantStoreData.phone,
			couponId: this.state.merchantStoreData.couponId,
			cuisine: this.state.merchantStoreData.cuisine,
			deliverySettingsId: this.state.merchantStoreData.deliverySettingsId,
			description: this.state.merchantStoreData.description,
			employeeIds: this.state.merchantStoreData.employeeIds, 
			extRestaurantId: this.state.merchantStoreData.extRestaurantId,
			paymentModelId: this.state.merchantStoreData.paymentModelId,
			pickupSettingsId : this.state.merchantStoreData.pickupSettingsId,
			siteSubDomain: this.state.merchantStoreData.siteSubDomain,
			siteUrl: this.state.merchantStoreData.siteUrl,
			storeName: this.state.merchantStoreData.storeName,
			supplierId: this.state.merchantStoreData.supplierId,
			tipsEnabled: this.state.merchantStoreData.tipsEnabled,
			website: this.state.merchantStoreData.website,
			id:this.state.merchantStoreData.id,
		};	

      	  const restaurantPut = JSON.stringify(merchantSettingsContent);

      	  axios.put(`http://159.65.134.223:8089/restaurant/${merchantSettingsContent.id}/`,restaurantPut,
          {headers: {"Content-Type": "Application/json"}},
          ).then((response) => { 
              this.setState({showSuccessMessage: response.message});
          }).catch(function(error){
          	console.log("error.message",error.message);
          	// this.setState({showSuccessMessage : error.message });
          });
      	}

      	activateTips=(e)=>{
       		this.setState({showTips:e.target.checked});
      	}

    changeHandler=(e)=>{
         console.log(e);
    }


	render(){ 

		const {merchantStoreData} = this.state; 

		return(
			
			<div>
				<StoreSetting merchantStoreData={merchantStoreData}
				submitMerchantSettings={this.submitMerchantSettings}
                showSuccessMessage={this.state.showSuccessMessage}
                              changeHandler={this.changeHandler}
				/>
				<div className={[classes.paper,'p-2 mt-4'].join(' ')}>
					<PaymentSetting merchantStoreData={merchantStoreData}  
					activateTips={(e)=>this.activateTips(e)}
					/>
				</div>
				{this.state.showTips?
				<div className={[classes.paper,'p-2 mt-4'].join(' ')}>
					<Tips/>
				</div>:''}
			</div>
		);
	}	
}

export default Storeettings;