import React,{Component} from 'react'; 
import AddressSetting from '../../components/AddressSetting/AddressSetting'; 
// import MapContainer from '../../components/AddressSetting/AddressSetting'; 
import classes from './AddressSettings.css';
import axios from 'axios';

class AddressSettings extends Component{

	state = {
		defaultAddress : '',
	}

	componentDidMount() {

        axios.get('http://159.65.134.223:8080/restaurant/1/address/')
            .then((response) => {
                console.log("response", response.data);
                this.setState({defaultAddress: response.data});
            }).catch(function (error) {
            console.log("error.message", error.message);
            // this.setState({defaultAddress: "Network Error!PLease try after some time."});
        });
    }
  
	render(){ 
		debugger;
		return( 
			<div className="col-md-12">
				<AddressSetting defaultAddress={this.state.defaultAddress} />
			</div>
		);
	}	
}

export default AddressSettings;