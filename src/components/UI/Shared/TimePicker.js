import React,{Component} from 'react';
import TimePicker from 'react-bootstrap-time-picker';


class Parent extends Component {
  constructor() {
    super();

    this.handleTimeChange = this.handleTimeChange.bind(this);

    this.state = { time: 0 };
  }

  handleTimeChange(time) {
        this.setState({ time : time });
  }

  render() {
    return <TimePicker onChange={this.handleTimeChange} value={this.state.time} />;
  }
}

export default Parent;
