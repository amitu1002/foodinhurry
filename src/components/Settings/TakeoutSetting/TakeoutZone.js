import React,{Component} from 'react';
import { GoogleApiWrapper, InfoWindow, Map, Marker } from 'google-maps-react';
import pinMarker from '../../../assets/images/pin.png';
import {geocodeByAddress, getLatLng} from "react-places-autocomplete";
import axios from "axios";

class TakeoutZone extends Component{

    constructor(props) {

        super(props);

        this.state = {
            address: '',
            center:{
                lat: 13.7261601,
                lng: 100.52493979999997,
            },
            zoom: 11,
            showingInfoWindow: true,
            activeMarker: {},
            selectedPlace: {},
            address1:'',
            address2:'',
            city:'',
            State:'',
            country:'',
            success:false,
            modalPopup:false,
            zone:{
                fee:'',
                distance:''
            },
            zoneData:{
                distance:'',
                fee:'',
            },
        };
    }

    handleChange = address => {
        this.setState({ address });
    };

    handleSelect = address => {
        const arrAddress = address.split(',');
        arrAddress.reverse();
        const city = arrAddress[2]?arrAddress[2]:'';
        const State = arrAddress[1]?arrAddress[1]:'';
        const country = arrAddress[0]?arrAddress[0]:'';

        geocodeByAddress(address)
            .then(results => getLatLng(results[0]))
            .then(
                (center) => {
                    this.setState(
                        {
                            center,
                            address,
                            country,
                            address1:address,
                            city:city,
                            State:State,
                            success:true,
                            showingInfoWindow:true,
                        }
                    );
                }
            )
            .catch(error => console.error('Error', error));
    };

    onMarkerClick = (props, marker, e) => {
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: true
        });
    }

    onMapClick = (props) => {
        if (this.state.showingInfoWindow) {
            this.setState({
                showingInfoWindow: false,
                activeMarker: null
            });
        }
    }

    onMarkerDragEnd = ( coord ) => {
        const { latLng } = coord;
        const lat = latLng.lat();
        const lng = latLng.lng();
        // this.updateAddressLatLong( lat, lng );
        const location = {
            lattitude : lat,
            longitude : lng,
        }
        const plottedAddress = {...this.props.defaultAddress, location };

        this.updateAddressLatLong( plottedAddress );
    };

    updateAddressLatLong = (plottedAddress)=>{

        const addressId = this.props.defaultAddress && this.props.defaultAddress.address.addressId;
        axios.put(`http://159.65.134.223:8080/restaurant/1/address/${plottedAddress.addressId}`, plottedAddress
        ).then(function(response){
            console.log( "response", response );
        }).catch(function(error){
            console.log("error.message",error.message);
        });
    }

    componentWillReceiveProps( nextProps ) {
        if(nextProps.defaultAddress && nextProps.defaultAddress.address && nextProps.defaultAddress.address.address1){
            this.setState({modalPopup:true});
        }
    }

    render(){

        const {center,zoom,address}=this.state;
        return(
            <div className="row">
                 <div className="card">
                     <Map
                         item
                         xs = { 12 }
                         google = {window.google}
                         onClick = { this.onMapClick }
                         zoom = { zoom }
                         style={{height:'80vh'}}
                         initialCenter = {center}
                     >
                         <Marker
                             onClick = { this.onMarkerClick }
                             title = { 'Address' }
                             position = {center}
                             name = { address }
                             options={{icon:`${pinMarker}`}}
                             draggable = {true}
                             onDragend={(t, map, coord) => this.onMarkerDragEnd(coord)}
                         />
                         <InfoWindow
                             marker = { this.state.activeMarker }
                             visible = { this.state.showingInfoWindow }
                         >
                             <h4>
                                 Address
                             </h4>
                             <p>
                                 {address}
                             </p>
                         </InfoWindow>
                     </Map>
                 </div>
            </div>
        );
    }
}

export default TakeoutZone;
