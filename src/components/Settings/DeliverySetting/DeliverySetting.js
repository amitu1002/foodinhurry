import React,{Component} from 'react';
import classes from '../../UI/Shared/Common.css';
import css from './DeliverySetting.css';
import TimePickers from '../../UI/Shared/TimePicker';
import DeliveryZone from './DeliveryZone';




class DeliverySetting extends Component{

  state = {
		weeks :null,
		sundayOpeningTimeArr :[],
		sundayDeliveryTimeArr :[],
      	mondayOpeningTimeArr:[],
		mondayDeliveryTimeArr :[],
		tuesdayOpeningTimeArr :[],
		tuesdayDeliveryTimeArr :[],
		wednesdayOpeningTimeArr :[],
		wednesdayDeliveryTimeArr :[],
		thursdayOpeningTimeArr :[],
		thursdayDeliveryTimeArr :[],
		fridayOpeningTimeArr :[],
		fridayDeliveryTimeArr :[],
		saturdayOpeningTimeArr :[],
		saturdayDeliveryTimeArr :[],
	}
	

	weeksChangeHandler = (e) =>  {
		this.setState({weeks:e.target.value});
	}

	openTimeChangeHandler = (e) => {
		this.setState ({openingTime:e.target.value});
	}


    closeTimeChangeHandler = (e) => {
        this.setState ({closingTime:e.target.value});
    }

    componentDidMount(){
        this.createItemSunday();
        this.createItemMonday();
        this.createItemTuesday();
        this.createItemWednesday();
        this.createItemThursday();
        this.createItemFriday();
        this.createItemSaturday();

    }

    createItemSunday =()=> {
        let openingtime = this.state.sundayOpeningTimeArr;
        let openingTimedata = openingtime.length + 1;
        openingtime.push({ openingTimedata });

        this.setState({
			sundayOpeningTimeArr: openingtime,
		});
    }


    weAreClosed=()=>{
  		this.setState({sundayOpeningTimeArr:[]});
	}

    removeItem = (e,item_id)=>{
  		e.preventDefault();
  		console.log("item_id",item_id);
        let el = document.getElementById(item_id);
        el.remove(); // Removes the div with the 'div-02' id
	}

    removeSubItem = (e,item_id)=>{
        e.preventDefault();
        console.log("item_id",item_id);
        let el = document.getElementById(item_id);
        el.remove(); // Removes the div with the 'div-02' id
    }

	addDeliveryItem=(e)=>{
        let openingtime = this.state.sundayDeliveryTimeArr;
        let openingTimedata = openingtime.length + 1;
        openingtime.push({ openingTimedata });

        this.setState({
            sundayDeliveryTimeArr: openingtime,
        });
	}

    createItemMonday =()=> {

        let openingtime = this.state.mondayOpeningTimeArr;
        let openingTimedata = openingtime.length + 1;
        openingtime.push({ openingTimedata });

        this.setState({
            mondayOpeningTimeArr: openingtime,
        });
    }


    weAreClosedMonday=()=>{
        this.setState({mondayOpeningTimeArr:[]});
    }

    removeItemMonday = (e,item_id)=>{
        e.preventDefault();
        console.log("item_id",item_id);
        let el = document.getElementById(item_id);
        el.remove(); // Removes the div with the 'div-02' id
    }

    removeSubItemMonday = (e,item_id)=>{
        e.preventDefault();
        console.log("item_id",item_id);
        let el = document.getElementById(item_id);
        el.remove(); // Removes the div with the 'div-02' id
    }

    addDeliveryItemMonday=(e)=>{
        let openingtime = this.state.mondayDeliveryTimeArr;
        let openingTimedata = openingtime.length + 1;
        openingtime.push({ openingTimedata });

        this.setState({
            mondayDeliveryTimeArr: openingtime,
        });
    }

    createItemTuesday =()=> {
        let openingtime = this.state.tuesdayOpeningTimeArr;
        let openingTimedata = openingtime.length + 1;
        openingtime.push({ openingTimedata });

        this.setState({
            tuesdayOpeningTimeArr: openingtime,
        });
    }


    weAreClosedTuesday=()=>{
        this.setState({tuesdayOpeningTimeArr:[]});
    }

    removeItemTuesday = (e,item_id)=>{
        e.preventDefault();
        console.log("item_id",item_id);
        let el = document.getElementById(item_id);
        el.remove(); // Removes the div with the 'div-02' id
    }

    removeSubItemTuesday = (e,item_id)=>{
        e.preventDefault();
        console.log("item_id",item_id);
        let el = document.getElementById(item_id);
        el.remove(); // Removes the div with the 'div-02' id
    }

    addDeliveryItemTuesday=(e)=>{
        let openingtime = this.state.tuesdayDeliveryTimeArr;
        let openingTimedata = openingtime.length + 1;
        openingtime.push({ openingTimedata });

        this.setState({
            tuesdayDeliveryTimeArr: openingtime,
        });
    }

    createItemWednesday =()=> {
        let openingtime = this.state.wednesdayOpeningTimeArr;
        let openingTimedata = openingtime.length + 1;
        openingtime.push({ openingTimedata });

        this.setState({
            wednesdayOpeningTimeArr: openingtime,
        });
    }


    weAreClosedWednesday=()=>{
        this.setState({wednesdayOpeningTimeArr:[]});
    }

    removeItemWednesday = (e,item_id)=>{
        e.preventDefault();
        console.log("item_id",item_id);
        let el = document.getElementById(item_id);
        el.remove(); // Removes the div with the 'div-02' id
    }

    removeSubItemWednesday = (e,item_id)=>{
        e.preventDefault();
        console.log("item_id",item_id);
        let el = document.getElementById(item_id);
        el.remove(); // Removes the div with the 'div-02' id
    }

    addDeliveryItemWednesday=(e)=>{
        let openingtime = this.state.wednesdayDeliveryTimeArr;
        let openingTimedata = openingtime.length + 1;
        openingtime.push({ openingTimedata });

        this.setState({
            wednesdayDeliveryTimeArr: openingtime,
        });
    }

    createItemThursday =()=> {
        let openingtime = this.state.thursdayOpeningTimeArr;
        let openingTimedata = openingtime.length + 1;
        openingtime.push({ openingTimedata });

        this.setState({
            thursdayOpeningTimeArr: openingtime,
        });
    }


    weAreClosedThursday=()=>{
        this.setState({thursdayOpeningTimeArr:[]});
    }

    removeItemThursday = (e,item_id)=>{
        e.preventDefault();
        console.log("item_id",item_id);
        let el = document.getElementById(item_id);
        el.remove(); // Removes the div with the 'div-02' id
    }

    removeSubItemThursday = (e,item_id)=>{
        e.preventDefault();
        console.log("item_id",item_id);
        let el = document.getElementById(item_id);
        el.remove(); // Removes the div with the 'div-02' id
    }

    addDeliveryItemThursday=(e)=>{
        let openingtime = this.state.thursdayDeliveryTimeArr;
        let openingTimedata = openingtime.length + 1;
        openingtime.push({ openingTimedata });

        this.setState({
            thursdayDeliveryTimeArr: openingtime,
        });
    }

    createItemFriday =()=> {
        let openingtime = this.state.fridayOpeningTimeArr;
        let openingTimedata = openingtime.length + 1;
        openingtime.push({ openingTimedata });

        this.setState({
            fridayOpeningTimeArr: openingtime,
        });
    }


    weAreClosedFriday=()=>{
        this.setState({fridayOpeningTimeArr:[]});
    }

    removeItemFriday = (e,item_id)=>{
        e.preventDefault();
        console.log("item_id",item_id);
        let el = document.getElementById(item_id);
        el.remove(); // Removes the div with the 'div-02' id
    }

    removeSubItemFriday = (e,item_id)=>{
        e.preventDefault();
        console.log("item_id",item_id);
        let el = document.getElementById(item_id);
        el.remove(); // Removes the div with the 'div-02' id
    }

    addDeliveryItemFriday=(e)=>{
        let openingtime = this.state.fridayDeliveryTimeArr;
        let openingTimedata = openingtime.length + 1;
        openingtime.push({ openingTimedata });

        this.setState({
            fridayDeliveryTimeArr: openingtime,
        });
    }

    createItemSaturday =()=> {
        let openingtime = this.state.saturdayOpeningTimeArr;
        let openingTimedata = openingtime.length + 1;
        openingtime.push({ openingTimedata });

        this.setState({
            saturdayOpeningTimeArr: openingtime,
        });
    }


    weAreClosedSaturday=()=>{
        this.setState({saturdayOpeningTimeArr:[]});
    }

    removeItemSaturday = (e,item_id)=>{
        e.preventDefault();
        console.log("item_id",item_id);
        let el = document.getElementById(item_id);
        el.remove(); // Removes the div with the 'div-02' id
    }

    removeSubItemSaturday = (e,item_id)=>{
        e.preventDefault();
        console.log("item_id",item_id);
        let el = document.getElementById(item_id);
        el.remove(); // Removes the div with the 'div-02' id
    }

    addDeliveryItemSaturday=(e)=>{
        let openingtime = this.state.saturdayDeliveryTimeArr;
        let openingTimedata = openingtime.length + 1;
        openingtime.push({ openingTimedata });

        this.setState({
            saturdayDeliveryTimeArr: openingtime,
        });
    }

	render(){

		return(
			<div>
				<div className="row p-2">
					<div className="col-md-6">
					    <div className="form-group">
						    <input type="checkbox" id="id-name--2" 
							name="set-name" className={classes.switchInput} />
							<label htmlFor="id-name--2" className={classes.switchLabel}>
							Accept Delivery Orders 
							</label> 
						<div className="form-row">
						    <label htmlFor="inputPassword" className="col-form-label">
						    Minimum Order Total $</label>
					    <div className="col-sm-2">
					      <input type="test" className="form-control" placeholder=""/>
					    </div>
					  </div>						
					</div>
				</div>					
					<div className="col-md-6">	  
					    <div className="form-group">
						    <input type="checkbox" id="id-name--3" 
							name="set-name" className={classes.switchInput} />
							<label htmlFor="id-name--3" className={classes.switchLabel}>
							Customers can place advance orders  
							</label> 
							<div className="col-md-6">
						   		<div className="form-group row">
						    		<label htmlFor="inputPassword" className="col-form-">
						    		Customers can place orders</label>					    
					  			</div>
							</div>	
							<div className="form-group row col-md-6">						
							    <select className="form-control col-md-3">
							    	<option>1</option>
							    	<option>2</option>
							    	<option>3</option>
							    	<option>4</option>
							    	<option>5</option>
							    </select>
							     <label htmlFor="formGroupExampleInput" className="col-md-8">
							     Days in advance</label>
							</div>
							<div className="form-group row col-md-12">
								<label htmlFor="formGroupExampleInput" className="col-md-2">
							     Notify me</label>						
							    <select className="form-control col-md-2">
							    	<option>1</option>
							    	<option>2</option>
							    	<option>3</option>
							    	<option>4</option>
							    	<option>5</option>
							    </select>
							     <label htmlFor="formGroupExampleInput" className="col-md-5">
							     minutes before order due</label>
							</div>
						</div> 
					</div>
				</div>
				<div className="form-row p-2">
					<div className="col-md-12">
					    <div className="form-group">					
					     <label htmlFor="formGroupExampleInput" 
					     className={[classes.css,'col-md-8'].join(' ')}>
					     Delivery Hours</label>
					    </div>
					</div>
					<div className="col-md-12">
                        <div className="form-group">
                            <a className="btn btn-secondary btn-sm m-2" data-toggle="collapse" href="#collapseExample" role="button"
                               aria-expanded="false" aria-controls="collapseExample">
                                Sunday
                            </a>
                            <a className="btn btn-secondary btn-sm m-2" data-toggle="collapse" href="#collapseExample1" role="button"
                               aria-expanded="false" aria-controls="collapseExample1">
                                Monday
                            </a>
                            <a className="btn btn-secondary btn-sm m-2" data-toggle="collapse" href="#tuesday" role="button"
                               aria-expanded="false" aria-controls="tuesday">
                                Tuesday
                            </a>
                            <a className="btn btn-secondary btn-sm m-2" data-toggle="collapse" href="#wednesday" role="button"
                               aria-expanded="false" aria-controls="wednesday">
                                Wednesday
                            </a>
                            <a className="btn btn-secondary btn-sm m-2" data-toggle="collapse" href="#thursday" role="button"
                               aria-expanded="false" aria-controls="thursday">
                                Thursday
                            </a>
                            <a className="btn btn-secondary btn-sm m-2" data-toggle="collapse" href="#friday" role="button"
                               aria-expanded="false" aria-controls="friday">
                                Friday
                            </a>
                            <a className="btn btn-secondary btn-sm m-2" data-toggle="collapse" href="#saturday" role="button"
                               aria-expanded="false" aria-controls="saturday">
                                Saturday
                            </a>
                        </div>
                        <div className="collapse" id="collapseExample">
                            <div className="card card-body">
							<div className="form-group">
								<div className="row">
									<a onClick={this.createItemSunday}
									   className="btn btn-primary m-2"><i className="text-white fa fa-plus"></i></a>
									<a onClick={this.weAreClosed}
									   className="btn btn-danger m-2 text-white"> We are closed </a>
								</div>
                            </div>
                                <div className="card">
                                    {this.state.sundayOpeningTimeArr.map((item, index) => {
                                        return (
									<div>
										<div className="form-inline p-2" id={`opening-${index}`} key={`opening-${index}`}>
											<div className="form-group mx-sm-3 mb-2">
												<label>Opening Hours</label>
											</div>
											<div className="form-group mx-sm-3 mb-2">
												<TimePickers start="10:00" end="21:00" step={30}
															 onChange ={this.closeTimeChangeHandler}/>
											</div>
											<div className="form-group mx-sm-3 mb-2">
												<TimePickers start="10:00" end="21:00" step={30}
															 onChange ={this.closeTimeChangeHandler}/>
											</div>
											<a href="javascript:;" onClick={(e) =>this.removeItem(e,'opening-'+index)}
											   className="m-2"><i className="fa fa-trash"></i></a>
										</div>

										<div className="form-inline p-2" id={`delivery-${index}`} key={`delivery-${index}`}>
											<div className="form-group mx-sm-3 mb-2">
												<label>Delivery Time </label>
											</div>
											<div className="form-group mx-sm-3 mb-2">
												<TimePickers start="10:00" end="21:00" step={30}
															 onChange={this.closeTimeChangeHandler}/>
											</div>
											<div className="form-group mx-sm-3 mb-2">
												<TimePickers start="10:00" end="21:00" step={30}
															 onChange={this.closeTimeChangeHandler}/>
											</div>
											<div className="form-group mx-sm-3 mb-2">
												<TimePickers start="10:00" end="21:00" step={30}
															 onChange={this.closeTimeChangeHandler}/>
											</div>
											<a href="javascript:;" onClick={(e)=>this.addDeliveryItem(e)}
											   className="m-2"><i className="fa fa-plus"></i></a>
											<a href="javascript:;"
											   onClick={(e) =>this.removeItem(e,'delivery-'+index)}
											   className="m-2"
											>
												<i className="fa fa-trash"></i>
											</a>
										</div>
                                        {this.state.sundayDeliveryTimeArr.map((index) => {
                                        	debugger;
                                            return (
                                                <div className="form-inline p-2" id={`adelivery-${index}`} key={`adelivery-${index}`}>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <label>Delivery Time </label>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <a href="javascript:;"
                                                       onClick={(e) =>this.removeSubItem(e,'adelivery-'+index)}
                                                       className="m-2"
                                                    >
                                                        <i className="fa fa-trash"></i>
                                                    </a>
                                                </div>
                                            );
                                        })}
									</div>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                        <div className="collapse" id="collapseExample1">
                            <div className="card card-body">
                                <div className="form-group">
                                    <div className="row">
                                        <a onClick={this.createItemMonday}
                                           className="btn btn-primary m-2"><i className="text-white fa fa-plus"></i></a>
                                        <a onClick={this.weAreClosedMonday}
                                           className="btn btn-danger m-2 text-white"> We are closed </a>
                                    </div>
                                </div>
                                <div className="card">
                                    {this.state.mondayOpeningTimeArr && this.state.mondayOpeningTimeArr.map((item, index) => {
                                        return (
                                            <div>
                                                <div className="form-inline p-2" id={`opening-${index}`} key={`opening-${index}`}>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <label>Opening Hours</label>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange ={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange ={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <a href="javascript:;" onClick={(e) =>this.removeItemMonday(e,'opening-'+index)}
                                                       className="m-2"><i className="fa fa-trash"></i></a>
                                                </div>

                                                <div className="form-inline p-2" id={`delivery-${index}`} key={`delivery-${index}`}>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <label>Delivery Time </label>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <a href="javascript:;" onClick={(e)=>this.addDeliveryItemMonday(e)}
                                                       className="m-2"><i className="fa fa-plus"></i></a>
                                                    <a href="javascript:;"
                                                       onClick={(e) =>this.removeItem(e,'delivery-'+index)}
                                                       className="m-2"
                                                    >
                                                        <i className="fa fa-trash"></i>
                                                    </a>
                                                </div>
                                                {this.state.mondayDeliveryTimeArr.map((index) => {
                                                    debugger;
                                                    return (
                                                        <div className="form-inline p-2" id={`adelivery-${index}`} key={`adelivery-${index}`}>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <label>Delivery Time </label>
                                                            </div>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <TimePickers start="10:00" end="21:00" step={30}
                                                                             onChange={this.closeTimeChangeHandler}/>
                                                            </div>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <TimePickers start="10:00" end="21:00" step={30}
                                                                             onChange={this.closeTimeChangeHandler}/>
                                                            </div>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <TimePickers start="10:00" end="21:00" step={30}
                                                                             onChange={this.closeTimeChangeHandler}/>
                                                            </div>
                                                            <a href="javascript:;"
                                                               onClick={(e) =>this.removeSubItemMonday(e,'adelivery-'+index)}
                                                               className="m-2"
                                                            >
                                                                <i className="fa fa-trash"></i>
                                                            </a>
                                                        </div>
                                                    );
                                                })}
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                        <div className="collapse" id="tuesday">

                            <div className="card card-body">
                                <div className="form-group">
                                    <div className="row">
                                        <a onClick={this.createItemTuesday}
                                           className="btn btn-primary m-2"><i className="text-white fa fa-plus"></i></a>
                                        <a onClick={this.weAreClosedTuesday}
                                           className="btn btn-danger m-2 text-white"> We are closed </a>
                                    </div>
                                </div>
                                <div className="card">
                                    {this.state.tuesdayOpeningTimeArr && this.state.tuesdayOpeningTimeArr.map((item, index) => {
                                        return (
                                            <div>
                                                <div className="form-inline p-2" id={`opening-${index}`} key={`opening-${index}`}>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <label>Opening Hours</label>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange ={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange ={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <a href="javascript:;" onClick={(e) =>this.removeItemTuesday(e,'opening-'+index)}
                                                       className="m-2"><i className="fa fa-trash"></i></a>
                                                </div>

                                                <div className="form-inline p-2" id={`delivery-${index}`} key={`delivery-${index}`}>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <label>Delivery Time </label>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <a href="javascript:;" onClick={(e)=>this.addDeliveryItemTuesday(e)}
                                                       className="m-2"><i className="fa fa-plus"></i></a>
                                                    <a href="javascript:;"
                                                       onClick={(e) =>this.removeItemTuesday(e,'delivery-'+index)}
                                                       className="m-2"
                                                    >
                                                        <i className="fa fa-trash"></i>
                                                    </a>
                                                </div>
                                                {this.state.mondayDeliveryTimeArr.map((index) => {
                                                    debugger;
                                                    return (
                                                        <div className="form-inline p-2" id={`adelivery-${index}`} key={`adelivery-${index}`}>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <label>Delivery Time </label>
                                                            </div>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <TimePickers start="10:00" end="21:00" step={30}
                                                                             onChange={this.closeTimeChangeHandler}/>
                                                            </div>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <TimePickers start="10:00" end="21:00" step={30}
                                                                             onChange={this.closeTimeChangeHandler}/>
                                                            </div>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <TimePickers start="10:00" end="21:00" step={30}
                                                                             onChange={this.closeTimeChangeHandler}/>
                                                            </div>
                                                            <a href="javascript:;"
                                                               onClick={(e) =>this.removeSubItemTuesday(e,'adelivery-'+index)}
                                                               className="m-2"
                                                            >
                                                                <i className="fa fa-trash"></i>
                                                            </a>
                                                        </div>
                                                    );
                                                })}
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                        <div className="collapse" id="wednesday">
                            <div className="card card-body">
                                <div className="form-group">
                                    <div className="row">
                                        <a onClick={this.createItemWednesday}
                                           className="btn btn-primary m-2"><i className="text-white fa fa-plus"></i></a>
                                        <a onClick={this.weAreClosedWednesday}
                                           className="btn btn-danger m-2 text-white"> We are closed </a>
                                    </div>
                                </div>
                                <div className="card">
                                    {this.state.wednesdayOpeningTimeArr && this.state.wednesdayOpeningTimeArr.map((item, index) => {
                                        return (
                                            <div>
                                                <div className="form-inline p-2" id={`opening-${index}`} key={`opening-${index}`}>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <label>Opening Hours</label>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange ={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange ={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <a href="javascript:;" onClick={(e) =>this.removeItemWednesday(e,'opening-'+index)}
                                                       className="m-2"><i className="fa fa-trash"></i></a>
                                                </div>

                                                <div className="form-inline p-2" id={`delivery-${index}`} key={`delivery-${index}`}>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <label>Delivery Time </label>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <a href="javascript:;" onClick={(e)=>this.addDeliveryItemWednesday(e)}
                                                       className="m-2"><i className="fa fa-plus"></i></a>
                                                    <a href="javascript:;"
                                                       onClick={(e) =>this.removeItemWednesday(e,'delivery-'+index)}
                                                       className="m-2"
                                                    >
                                                        <i className="fa fa-trash"></i>
                                                    </a>
                                                </div>
                                                {this.state.wednesdayDeliveryTimeArr.map((index) => {
                                                    debugger;
                                                    return (
                                                        <div className="form-inline p-2" id={`adelivery-${index}`} key={`adelivery-${index}`}>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <label>Delivery Time </label>
                                                            </div>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <TimePickers start="10:00" end="21:00" step={30}
                                                                             onChange={this.closeTimeChangeHandler}/>
                                                            </div>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <TimePickers start="10:00" end="21:00" step={30}
                                                                             onChange={this.closeTimeChangeHandler}/>
                                                            </div>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <TimePickers start="10:00" end="21:00" step={30}
                                                                             onChange={this.closeTimeChangeHandler}/>
                                                            </div>
                                                            <a href="javascript:;"
                                                               onClick={(e) =>this.removeSubItemWednesday(e,'adelivery-'+index)}
                                                               className="m-2"
                                                            >
                                                                <i className="fa fa-trash"></i>
                                                            </a>
                                                        </div>
                                                    );
                                                })}
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                        <div className="collapse" id="thursday">
                            <div className="card card-body">
                                <div className="form-group">
                                    <div className="row">
                                        <a onClick={this.createItemThursday}
                                           className="btn btn-primary m-2"><i className="text-white fa fa-plus"></i></a>
                                        <a onClick={this.weAreClosedThursday}
                                           className="btn btn-danger m-2 text-white"> We are closed </a>
                                    </div>
                                </div>
                                <div className="card">
                                    {this.state.thursdayOpeningTimeArr && this.state.thursdayOpeningTimeArr.map((item, index) => {
                                        return (
                                            <div>
                                                <div className="form-inline p-2" id={`opening-${index}`} key={`opening-${index}`}>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <label>Opening Hours</label>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange ={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange ={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <a href="javascript:;" onClick={(e) =>this.removeItemThursday(e,'opening-'+index)}
                                                       className="m-2"><i className="fa fa-trash"></i></a>
                                                </div>

                                                <div className="form-inline p-2" id={`delivery-${index}`} key={`delivery-${index}`}>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <label>Delivery Time </label>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <a href="javascript:;" onClick={(e)=>this.addDeliveryItemThursday(e)}
                                                       className="m-2"><i className="fa fa-plus"></i></a>
                                                    <a href="javascript:;"
                                                       onClick={(e) =>this.removeItemThursday(e,'delivery-'+index)}
                                                       className="m-2"
                                                    >
                                                        <i className="fa fa-trash"></i>
                                                    </a>
                                                </div>
                                                {this.state.thursdayDeliveryTimeArr.map((index) => {
                                                    debugger;
                                                    return (
                                                        <div className="form-inline p-2" id={`adelivery-${index}`} key={`adelivery-${index}`}>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <label>Delivery Time </label>
                                                            </div>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <TimePickers start="10:00" end="21:00" step={30}
                                                                             onChange={this.closeTimeChangeHandler}/>
                                                            </div>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <TimePickers start="10:00" end="21:00" step={30}
                                                                             onChange={this.closeTimeChangeHandler}/>
                                                            </div>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <TimePickers start="10:00" end="21:00" step={30}
                                                                             onChange={this.closeTimeChangeHandler}/>
                                                            </div>
                                                            <a href="javascript:;"
                                                               onClick={(e) =>this.removeSubItemThursday(e,'adelivery-'+index)}
                                                               className="m-2"
                                                            >
                                                                <i className="fa fa-trash"></i>
                                                            </a>
                                                        </div>
                                                    );
                                                })}
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                        <div className="collapse" id="friday">
                            <div className="card card-body">
                                <div className="form-group">
                                    <div className="row">
                                        <a onClick={this.createItemFriday}
                                           className="btn btn-primary m-2"><i className="text-white fa fa-plus"></i></a>
                                        <a onClick={this.weAreClosedFriday}
                                           className="btn btn-danger m-2 text-white"> We are closed </a>
                                    </div>
                                </div>
                                <div className="card">
                                    {this.state.fridayOpeningTimeArr && this.state.fridayOpeningTimeArr.map((item, index) => {
                                        return (
                                            <div>
                                                <div className="form-inline p-2" id={`opening-${index}`} key={`opening-${index}`}>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <label>Opening Hours</label>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange ={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange ={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <a href="javascript:;" onClick={(e) =>this.removeItemFriday(e,'opening-'+index)}
                                                       className="m-2"><i className="fa fa-trash"></i></a>
                                                </div>

                                                <div className="form-inline p-2" id={`delivery-${index}`} key={`delivery-${index}`}>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <label>Delivery Time </label>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <a href="javascript:;" onClick={(e)=>this.addDeliveryItemFriday(e)}
                                                       className="m-2"><i className="fa fa-plus"></i></a>
                                                    <a href="javascript:;"
                                                       onClick={(e) =>this.removeItemFriday(e,'delivery-'+index)}
                                                       className="m-2"
                                                    >
                                                        <i className="fa fa-trash"></i>
                                                    </a>
                                                </div>
                                                {this.state.fridayDeliveryTimeArr.map((index) => {
                                                    debugger;
                                                    return (
                                                        <div className="form-inline p-2" id={`adelivery-${index}`} key={`adelivery-${index}`}>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <label>Delivery Time </label>
                                                            </div>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <TimePickers start="10:00" end="21:00" step={30}
                                                                             onChange={this.closeTimeChangeHandler}/>
                                                            </div>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <TimePickers start="10:00" end="21:00" step={30}
                                                                             onChange={this.closeTimeChangeHandler}/>
                                                            </div>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <TimePickers start="10:00" end="21:00" step={30}
                                                                             onChange={this.closeTimeChangeHandler}/>
                                                            </div>
                                                            <a href="javascript:;"
                                                               onClick={(e) =>this.removeSubItemFriday(e,'adelivery-'+index)}
                                                               className="m-2"
                                                            >
                                                                <i className="fa fa-trash"></i>
                                                            </a>
                                                        </div>
                                                    );
                                                })}
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                        <div className="collapse" id="saturday">
                            <div className="card card-body">
                                <div className="form-group">
                                    <div className="row">
                                        <a onClick={this.createItemSaturday}
                                           className="btn btn-primary m-2"><i className="text-white fa fa-plus"></i></a>
                                        <a onClick={this.weAreClosedSaturday}
                                           className="btn btn-danger m-2 text-white"> We are closed </a>
                                    </div>
                                </div>
                                <div className="card">
                                    {this.state.saturdayOpeningTimeArr && this.state.saturdayOpeningTimeArr.map((item, index) => {
                                        return (
                                            <div>
                                                <div className="form-inline p-2" id={`opening-${index}`} key={`opening-${index}`}>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <label>Opening Hours</label>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange ={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange ={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <a href="javascript:;" onClick={(e) =>this.removeItemSaturday(e,'opening-'+index)}
                                                       className="m-2"><i className="fa fa-trash"></i></a>
                                                </div>

                                                <div className="form-inline p-2" id={`delivery-${index}`} key={`delivery-${index}`}>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <label>Delivery Time </label>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <div className="form-group mx-sm-3 mb-2">
                                                        <TimePickers start="10:00" end="21:00" step={30}
                                                                     onChange={this.closeTimeChangeHandler}/>
                                                    </div>
                                                    <a href="javascript:;" onClick={(e)=>this.addDeliveryItemSaturday(e)}
                                                       className="m-2"><i className="fa fa-plus"></i></a>
                                                    <a href="javascript:;"
                                                       onClick={(e) =>this.removeItemSaturday(e,'delivery-'+index)}
                                                       className="m-2"
                                                    >
                                                        <i className="fa fa-trash"></i>
                                                    </a>
                                                </div>
                                                {this.state.saturdayDeliveryTimeArr.map((index) => {
                                                    return (
                                                        <div className="form-inline p-2" id={`adelivery-${index}`} key={`adelivery-${index}`}>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <label>Delivery Time </label>
                                                            </div>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <TimePickers start="10:00" end="21:00" step={30}
                                                                             onChange={this.closeTimeChangeHandler}/>
                                                            </div>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <TimePickers start="10:00" end="21:00" step={30}
                                                                             onChange={this.closeTimeChangeHandler}/>
                                                            </div>
                                                            <div className="form-group mx-sm-3 mb-2">
                                                                <TimePickers start="10:00" end="21:00" step={30}
                                                                             onChange={this.closeTimeChangeHandler}/>
                                                            </div>
                                                            <a href="javascript:;"
                                                               onClick={(e) =>this.removeSubItemSaturday(e,'adelivery-'+index)}
                                                               className="m-2"
                                                            >
                                                                <i className="fa fa-trash"></i>
                                                            </a>
                                                        </div>
                                                    );
                                                })}
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>

					</div>
					{/*Map div starts here*/}
					<div className="card p-2 m-2 col-md-12">
						 <DeliveryZone />
					</div>
                    {/*Map div ends here*/}
				</div>
			</div>
		)
	}
}

export default DeliverySetting;