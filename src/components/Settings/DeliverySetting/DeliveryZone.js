import React,{Component} from 'react';

class DeliveryZone extends Component{

    state={
        zone:{
            fee:'',
            distance:''
        },
        zoneData:{
            distance:'',
            fee:'',
        },

    }

    addZone=()=>{
        debugger;
        let zone = this.state.zone;
         zone = {...zone,distance : this.state.zoneData.distance,
            fee : this.state.zoneData.fee};

        this.setState({
            zone
        });
    }

    removeZone=()=>{

    }

    handleChange=(e,name)=>{
        // debugger;
        const zoneData = this.state.zoneData;
        zoneData[name] = e.target.value;
        this.setState({
            zoneData
        });
    }

    render(){

        console.log("state",this.state);

        return(
            <div className="row">
            <div className="col-6">
                 <div className="form-inline p-2">
                      <label>Accept delivery orders form customers within </label>
                      <select className="form-control">
                          <option>5 Miles</option>
                          <option>10 Miles</option>
                          <option>15 Miles</option>
                          <option>20 Miles</option>
                          <option>25 Miles</option>
                          <option>30 Miles</option>
                          <option>35 Miles</option>
                      </select>
                 </div>
                <div className="form-inline p-2">
                    <label>Delivery Settings</label>
                    <div className="form-check form-check-inline">
                        <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1"
                               value="option1" />
                            <label className="form-check-label" htmlFor="inlineRadio1">Radius</label>
                    </div>
                    <div className="form-check form-check-inline">
                        <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2"
                               value="option2"/>
                            <label className="form-check-label" htmlFor="inlineRadio2">Zones</label>
                    </div>
                </div>
                <div className="form-inline">
                    <label className="my-1 m-2" htmlFor="inlineFormCustomSelectPref"> Distance </label>
                    <input type="text" className="form-control col-md-3" name="distance" ref="distance"
                           onChange={(e)=>this.handleChange(e,'distance')} value={this.state.distance} />
                    <label className="my-1 m-2" htmlFor="inlineFormCustomSelectPref"> Fee </label>
                    <input type="text" className="form-control col-md-3" name="fee"
                           onChange={(e)=>this.handleChange(e,'fee')}
                           value={this.state.fee} ref="fee" />
                    <button type="submit" onClick={this.addZone}
                            className="btn btn-primary my-1 m-2">Add Zone</button>
                </div>
                <div className="table table-bordered table-hover">
                    {this.state.zone && Object.keys(this.state.zone).map((key)=>{
                        <div>{this.state.zone[key]}</div>
                    })}
                </div>
            </div>
                <div className="col-6">
                     <div className="card">
                         Map
                     </div>

                </div>
            </div>
        );
    }
}

export default DeliveryZone;
