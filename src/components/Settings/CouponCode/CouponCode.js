import React,{Component} from 'react';
import classes from '../../UI/Shared/Common.css';

class CouponCode extends Component{
	render(){
		return(
		<div>
				<div className="row">
					<div className="col-md-12"> 
						<input type="checkbox" id="id-name--2" 
						name="set-name" className={classes.switchInput} />
						<label for="id-name--2" className={classes.switchLabel}>
						Status </label>
						<button type="button" className="float-right btn btn-primary" >Save Coupons</button>
					</div>
				</div> 
				<div className="row p-2">

					<div className="col-md-6">
					    <div className="form-group">
						    <label for="formGroupExampleInput">Coupon Name</label>
						    <input type="text" className="form-control col-md-10"
						     id="formGroupExampleInput" placeholder="" />
						</div>
					</div>
					<div className="col-md-6">	  
					    <div className="form-group">
						    <label for="formGroupExampleInput">Coupon Code</label>
						    <input type="text" className="form-control col-md-10"
						     id="formGroupExampleInput" placeholder="" />
						</div> 
					</div>
					<div className="col-md-6 form-row p-2" style={{margin:'auto', margin:'1px'}}>	  
					    <div className="form-group col-md-6">
						    <label for="formGroupExampleInput">Amount</label>
						    <input type="text" className="form-control col-md-10"
						     id="formGroupExampleInput" placeholder="" />
						</div> 
						<div className="form-group col-md-6">
						    <label for="formGroupExampleInput">Discount Type</label>
						    <select className="form-control">
						    	<option>1</option>
						    	<option>2</option>
						    	<option>3</option>
						    	<option>4</option>
						    	<option>5</option>
						    </select>
						</div>
					</div>  
				</div> 
				<div className="row p-2">
					<div className="col-md-8 form-row">	  
					    <div className="form-group col-md-6">
						    <label for="formGroupExampleInput">Usage Type</label>
						    <select className="form-control">
						    	<option>1</option>
						    	<option>2</option>
						    	<option>3</option>
						    	<option>4</option>
						    	<option>5</option>
						    </select>
						</div> 
						<div className="form-group col-md-6">
						    <label for="formGroupExampleInput">How many times can a customer use the coupon ?</label>
						    <select className="form-control">
						    	<option>1</option>
						    	<option>2</option>
						    	<option>3</option>
						    	<option>4</option>
						    	<option>5</option>
						    </select>
						</div>
					</div>
				</div>
				<div className="row p-1">
					<div className="col-md-6">
					    <div className="form-group" style={{margin:'auto', margin:'1px'}}>
						    <label for="formGroupExampleInput">Minimum Purchase Amount</label>
						    <input type="text" className="form-control col-md-10"
						     id="formGroupExampleInput" placeholder="" />
						</div>
					</div>
					<div className="col-md-6">	  
					    <div className="form-group">
						    <label for="formGroupExampleInput">Maximum Redeem Points</label>
						    <input type="text" className="form-control col-md-10"
						     id="formGroupExampleInput" placeholder="" />
						</div> 
					</div>
				</div>	
		</div>
		)
		
	}
}

export default CouponCode;