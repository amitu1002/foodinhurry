import React,{Component} from 'react'; 
import { GoogleApiWrapper, InfoWindow, Map, Marker } from 'google-maps-react'; 
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete'; 
import pinMarker from '../../assets/images/pin.png';
import Modal from '../UI/Modal/Modal';
import axios from "axios";
  
class AddressSetting extends Component{

	constructor(props) {
	    
	    super(props);
	    
	    this.state = { 
	    	address: '',
	    	center:{
	    		lat: 13.7261601,
				lng: 100.52493979999997,
			},
	    	zoom: 11,
	    	showingInfoWindow: true,
			activeMarker: {},
			selectedPlace: {},
			address1:'',
			address2:'',
			city:'',
			State:'',
            country:'',
			success:false,
			modalPopup:false,
			markerData:'',
	    };
	  }

	  handleChange = address => {
	    this.setState({ address });
	  };
	 
	  handleSelect = address => { 
	  	const arrAddress = address.split(','); 
		arrAddress.reverse();
		const city = arrAddress[2]?arrAddress[2]:'';
		const State = arrAddress[1]?arrAddress[1]:'';
        const country = arrAddress[0]?arrAddress[0]:'';

        geocodeByAddress(address)
	      .then(results => getLatLng(results[0]))
	      .then( 
	      		(center) => {   
	      			this.setState(
	      				{ 
	      					center,
	      					address,
							country,
	      					address1:address,
	      					city:city,
	      					State:State,
	      					success:true,
	      					showingInfoWindow:true,
	      				}
	      			);
	      		}
	      ) 
	      .catch(error => console.error('Error', error));
	  }; 

	  onMarkerClick = (props, marker, e) => {
	    this.setState({
	      selectedPlace: props,
	      activeMarker: marker,
	      showingInfoWindow: true
	    });
	  }
	  
	  onMapClick = (props) => {
	    if (this.state.showingInfoWindow) {
	      this.setState({
	        showingInfoWindow: false,
	        activeMarker: null
	      });
	    }
	  }

	  onMarkerDragEnd = ( coord ) => {
			const { latLng } = coord;
			const lat = latLng.lat();
			const lng = latLng.lng();
			// this.updateAddressLatLong( lat, lng );
		  const location = {
		  	lattitude : lat,
            longitude : lng,
		  }
          const plottedAddress = {...this.props.defaultAddress, location };
		  // console.log("plott",address);
		  this.setState({markerData: plottedAddress});
		  // this.updateAddressLatLong( plottedAddress );
	  };

	  updateAddressLatLong = ()=>{
		  const plottedAddress = this.state.markerData;
          const addressId = this.props.defaultAddress && this.props.defaultAddress.address.addressId;
          axios.put(`http://159.65.134.223:8080/restaurant/1/address/${plottedAddress.addressId}`, plottedAddress
		  ).then(function(response){
              console.log( "response", response );
          }).catch(function(error){
              console.log("error.message",error.message);
          });
	  }

    closeModal=()=>{
        this.refs.addressref.focus();
    	this.setState({modalPopup:false});
    }

    continueCloseModal=()=>{
        // this.refs.addressref.focus();
        this.setState({modalPopup:false});
        // this.updateAddressLatLong();
    }

    cancelCloseModal=()=>{
        this.setState({ modalPopup : false });
    }

    addressConfirmHandler = (e)=>{
	  	// debugger;
		// console.log("e",e.target.click);
        this.setState({modalPopup:true});
        // this.updateAddressLatLong();
	}

	render(){ 
		debugger;
		const {center,zoom,address,modalPopup}= this.state;

		const address1= this.props.defaultAddress && this.props.defaultAddress.address && this.props.defaultAddress.address.address1;
        const address2= this.props.defaultAddress && this.props.defaultAddress.address && this.props.defaultAddress.address.address2;
        const city= this.props.defaultAddress && this.props.defaultAddress.address && this.props.defaultAddress.address.city;
        const country= this.props.defaultAddress && this.props.defaultAddress.address && this.props.defaultAddress.address.country;
        const extAddressId= this.props.defaultAddress && this.props.defaultAddress.address && this.props.defaultAddress.address.extAddressId;
        const phone= this.props.defaultAddress && this.props.defaultAddress.address && this.props.defaultAddress.address.phone;
        const state= this.props.defaultAddress && this.props.defaultAddress.address && this.props.defaultAddress.address.state;
        const supplierId= this.props.defaultAddress && this.props.defaultAddress.address && this.props.defaultAddress.address.supplierId;
		const zip = this.props.defaultAddress && this.props.defaultAddress.address && this.props.defaultAddress.address.zip;
		const addressId = this.props.defaultAddress && this.props.defaultAddress.address && this.props.defaultAddress.address.addressId;

        return(
			
		<div className="row" style={{minHeight:'800px'}}>

                <Modal show={modalPopup} modalClosed={this.closeModal}>
                    Are you sure you want to plot this address as a location?
                    For more accuracy of the address,you can change the address pin to more accurate location to serve you the better.
                    DO you want to continue with this address?
					<div className="form-row well">
						<button className="btn btn-sm btn-primary" type="button" onClick={this.continueCloseModal}>Continue</button>
                        <button className="btn btn-sm btn-default" type="button" onClick={this.cancelCloseModal}>Cancel</button>
					</div>
                </Modal>

				 <div className="col-md-3">
					<form>
					  <div className="form-group">
					    <label for="formGroupExampleInput">Address</label>
					    <input name="address" ref="addressref" value={address1?address1:this.state.address}
							   type="text" className="form-control"/>
					  </div>
					  <div className="form-group">
					    <label for="formGroupExampleInput">Address2</label>
					    <input name="address2" type="text" value={address2?address2:''} className="form-control"/>
					  </div> 
					  <div className="form-group">
					    <label for="formGroupExampleInput">City</label>
					    <input name="city" value={city?city:this.state.city} type="text" className="form-control"/>
					  </div>
					  <div className="form-group">
					    <label for="formGroupExampleInput">State</label>
					    <input name="state" value={state?state:this.state.State} type="text" className="form-control"/>
					  </div>
					  <div className="form-group">
						<label htmlFor="formGroupExampleInput">Country</label>
						<input name="state" value={country ? country : this.state.country} type="text"
							   className="form-control"/>
					  </div>
					  <div className="form-group">
					    <label for="formGroupExampleInput">Zip Code</label>
					    <input name="zipcode" type="text" value={zip?zip:''} className="form-control"/>
					  </div>
					  <div className="form-group">
					    <label for="formGroupExampleInput">Phone Number</label>
					    <input name="phone_number" type="text" value={phone?phone:''} className="form-control"/>
					  </div>
					<div className="form-group">
						<button type="button" onClick={(e)=>this.addressConfirmHandler(e)}
								className="btn btn-primary" >Save Address</button>
					</div>
					</form>
			     </div>
			     <div className="col-md-9"> 
				 	<div style={{height:'500px'}}>
					 {this.state.success?<div class="alert alert-primary" role="alert">
						  Your Address has been set!
					 </div>:''}
					 <PlacesAutocomplete
					        value={this.state.address}
					        onChange={this.handleChange}
					        onSelect={this.handleSelect}
					      >
					        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
					          <div>
					            <input style={{width:'103%'}}
					              {...getInputProps({
					                placeholder : 'Search Places ...',
					                className : 'location-search-input form-control',
									ref : 'searchaddressref',
									name:'searchaddressref',
					              })}
					            />
					            <div className="autocomplete-dropdown-container">
					              {loading && <div>Loading...</div>}
					              {suggestions.map(suggestion => {
					                const className = suggestion.active
					                  ? 'suggestion-item--active'
					                  : 'suggestion-item';
					                // inline style for demonstration purpose
					                const style = suggestion.active
					                  ? { backgroundColor: '#fafafa', cursor: 'pointer' }
					                  : { backgroundColor: '#ffffff', cursor: 'pointer' };
					                return (
					                  <div
					                    {...getSuggestionItemProps(suggestion, {
					                      className,
					                      style,
					                    })}
					                  >
					                    <span>{suggestion.description}</span>
					                  </div>
					                );
					              })}
					            </div>
					   		</div>
				        )}
				      </PlacesAutocomplete>
			         <Map
				        item
				        xs = { 12 } 
				        google = {window.google}
				        onClick = { this.onMapClick }
				        zoom = { zoom }
				        style={{height:'80vh'}}
				        initialCenter = {center}
				      >
				        <Marker
				          onClick = { this.onMarkerClick }
				          title = { 'Address' }
				          position = {center}
				          name = { address }
				          options={{icon:`${pinMarker}`}}
				          draggable = {true}
				          onDragend={(t, map, coord) => this.onMarkerDragEnd(coord)}
				        />
				          <InfoWindow
				            marker = { this.state.activeMarker }
				            visible = { this.state.showingInfoWindow }
				          > 
				            <h4>
				              Address
				            </h4>
				            <p>
				              {address}
				            </p>
				        </InfoWindow>
				      </Map>
			     </div>  
			</div>		
			</div>
		);
	}	
}

export default AddressSetting;