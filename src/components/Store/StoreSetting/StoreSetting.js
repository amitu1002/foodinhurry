import React from 'react';
import classes from './StoreSetting.css';

const StoreSetting = (props)=> {
	debugger;
	const storeName = props.merchantStoreData && props.merchantStoreData.storeName?props.merchantStoreData.storeName:null;
	const siteSubDomain = props.merchantStoreData && props.merchantStoreData.siteSubDomain?props.merchantStoreData.siteSubDomain:null;

	return(
		<div>
			<div className="row">
				<div>{props.showSuccessMessage}</div>
				<div className="col-3 text-left">
					<h4> Find Food Quick Fee </h4>
				</div>
				<div className="col-9 ml-0 pl-0">
				 	<div className={[classes.customRadioPurple,"custom-control custom-radio"].join(' ')}>
					  <input type="radio" id="customRadio1" name="customRadio" className={[classes.customRadioPurple,"custom-control-input"].join(' ')} checked/>
					  <label className={[classes.lblcustomradio,"custom-control-label"].join(' ')} for="customRadio1">Customer Pays Online Order Fees </label>
					</div>
					<div className="custom-control custom-radio">
					  <input type="radio" id="customRadio2" name="customRadio" className="custom-control-input" />
					  <label className="custom-control-label" for="customRadio2">Merchant Pays Online Order Fees </label>
					</div>
				</div>
				<div className="col-md-12">
						<button type="button"
						style={{marginTop:'-50px'}}
						onClick={props.submitMerchantSettings}
						className="btn btn-primary float-right">Save Store Settings</button>
				</div>
			</div>
			<div className="form-row mt-4">
			    <div className="col-md-6">
			      <label for="validationCustomUsername">Business Name</label>
			      <div className="input-group">
			        <input type="text" className="form-control col-md-6"
			        value={storeName} onChange={(event) =>props.changeHandler(event)} placeholder="Business Name" required/>
			      </div>
			    </div>
			    <div className="col-md-6">
			      <label className="mt-2 mb-0">Store Url</label>
			      <div className="input-group mb-3 mt-3">
					  <span>http://</span>
					  <input type="text" value={siteSubDomain} style={{padding:'16px 0px 0px 1px'}} className={[classes.storeUrlTextField,"col-md-6"].join(' ')} />
					  <span>.quickfoodfast.com</span>
					</div>
			    </div>
			  </div>
			</div>
	);
}

export default StoreSetting;