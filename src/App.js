import React, { Component } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Layout from './hoc/Layout/Layout';
import StoreSettings from './containers/StoreSettings/StoreSettings';
import AddressSettings from './containers/AddressSettings/AddressSettings';
import CouponCodes from './containers/Settings/CouponCodes/CouponCodes';
import DeliverySettings from './containers/Settings/DeliverySettings/DeliverySettings';
import TakeoutSettings from './containers/Settings/TakeoutSettings/TakeoutSettings';
import querySearch from "query-string";

class App extends Component {
  state={
      redirectToClover : true,
  }
  componentDidMount() {
      debugger;
      const cloverParseData = querySearch.parse(this.props.location.search);  
      if( !cloverParseData.client_id && ( this.props.location.pathname === "/" ||
          this.props.location.pathname === "/restaurant") )
      {
          this.setCloverData();
      } 
  } 
  setCloverData = () =>{ 
      window.location.href = 'https://www.clover.com/oauth/authorize?client_id=M51RDN47M2ZHT&redirect_uri=http://localhost:8080/restaurant&response_type=code&state=cfxtKB';
  }
   
  render () {
    let routes = (
      <Switch> 
        <Route path="/" exact component={StoreSettings} />
        <Route path="/restaurant" component={StoreSettings} />
        <Route path="/AddressSettings" component={AddressSettings} />
        <Route path="/CouponCodes" component={CouponCodes} />
        <Route path="/DeliverySettings" component={DeliverySettings} />
        <Route path="/TakeoutSettings" component={TakeoutSettings} />
        <Redirect to="/" /> 
      </Switch>
    );  
    return (
      
      <div>
        <Layout>
          {routes}
        </Layout>
      </div>
    );
  }
} 

export default withRouter( App );
